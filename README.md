# REVMC.NET Twitter / Instagram Bot #

This bot uses the free resources of Google App Engine to send out 2 random tweets (one funny, one spiritual) every 12 hours.

This bot uses the same logic for Instagram as well.  Unfortunately, I couldn't figure out how to get it to work on Google App Engine (GAE has multiple limitations by design, ie. read-only file permissions), 
but am currently posting through Instagram locally using Windows Task Scheduler.

