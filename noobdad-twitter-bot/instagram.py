import fix_path
import os
import logging
import sys
import traceback
import random
import requests
from tweepy import *
from time import gmtime, strftime
from urllib2 import urlopen, URLError
from instapy_cli.cli import InstapyCli as client

def calculateCounter(year, days, hours):
    # creates a sequential counter based on time of call
    result =(int(year) * 365 * 2 + (int(days)-1) * 2 + (int(hours) / 12))
    # Multiplying by prime so pictures are not displayed consecutively - has the appearance of randomness
    result = result * 509
    return result

def calculateCounterFromTimestamp():
    days = int(strftime("%j"))
    year = int(strftime("%Y"))
    hours = int(strftime("%H"))
    counter = calculateCounter(year, days, hours)
    return counter

class AutoInstagram():    
    
    def get(self, counter=-1):
        if (counter == -1):
            counter = calculateCounterFromTimestamp()
        print("Counter:" + str(counter) + '<br>')
        
        try:
            print('Connecting to Instagram...<BR>')
            with open("NoobDadGaming.txt") as f:
                content = f.read().splitlines()
                # Consumer keys and access tokens, used for OAuth
                username = content[4]
                password = content[5]  
            
            def getFiles(path):
                directory = os.path.join(os.path.split(__file__)[0], path)
                listOfFiles = os.listdir(directory)
                return listOfFiles
            
            gameMemes = getFiles('images/games')
            gamesIndex = int(counter % len(gameMemes))
            print("gamesIndex: %d <BR>" % (gamesIndex))
            
            rawMessage = "- Have some #GamerMemes courtesy of REVMC.NET! -\n"
            print("gamesIndex: %d <BR>" % (gamesIndex))
            rawMessage += random.choice(['#OverMEME, #GAMING', '#NOOBS', '#NEWBS', '#OVER9000', '#GAMERS', '#GamersRock', '#FUNNY', '#awesome', '#gamer', '#gaming', '#lol', '#nerd', '#gfuel', '#game', '#gamingmeme', '#face', '#meme', '#memes', '#dank', '#dankmeme', '#GeeksUnite'])            
            
            #Convert message to utf-8 so Tweepy doesn't strip newlines
            message = rawMessage.decode('utf-8')    
                
            filename = os.path.join(os.path.split(__file__)[0], 'images/games', gameMemes[gamesIndex])
            print("Attempting tweet: " + filename)
            
            with client(username, password) as cli:
                text = message
                with open(filename) as f:
                    content = f.read().splitlines()
                
                cli.upload(filename)
            
            print("Tweeted " + message + " Attachment: " + filename + "<BR>")
            
            bibleImages = getFiles('images/bible')
            bibleIndex = int(counter % len(bibleImages))
            print("bibleIndex: %d <BR>" % (bibleIndex))
            rawMessage = "- Have a BLESSED DAY courtesy of REVMC.NET -\n"
            rawMessage += random.choice(['#GodRules, #GodIsLove', '#GodIsAwesome', '#Awesome', '#Love', '#LoveTrumpsHate', '#Beautiful', '#Salvation', '#JesusChrist', '#Jesus', '#KingOfKings', '#LordOfLords', '#JesusIsKing', '#Fuel', '#ChristHasRisen'])            
            
            #Convert message to utf-8 so Tweepy doesn't strip newlines
            message = rawMessage.decode('utf-8')
            filename = os.path.join(os.path.split(__file__)[0], 'images/bible', bibleImages[bibleIndex])
            print("Attempting tweet: " + filename + "<BR>")
            
            
            
            # api.uploadPhoto(filename, caption = message)
            print("Instagrammed " + message + " Attachment: " + filename)
            
            #foo = ['a', 'b', 'c', 'd', 'e']
            #from random import randrange
            #random_index = randrange(0,len(foo))
            #print foo[random_index]
            #message = random.choice(content)
            logging.info(message)

        except URLError:
            logging.error('URLError: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
            logging.error(message)
        except:
            logging.error('Unexpected error: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
            logging.error(traceback.print_exc())
            

def main():
    seed = -1
    if (len(sys.argv) > 1):
        seed = int(sys.argv[1])      
    
    api = AutoInstagram()
    api.get(seed)
    
if __name__ == '__main__':
    main()

