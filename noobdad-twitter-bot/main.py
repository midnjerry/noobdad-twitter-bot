import webapp2

MAIN_PAGE_HTML = """\
<html>
  <head>
    <title>NoobDad Joke Bot</title>
  </head>
  <body>
    <h3>NoobDad Joke Bot</h3>
    <p>This is the home of <a href="https://twitter.com/NoobDadGaming">@NoobDadGaming bot</a>.
       I'm a Twitter bot that posts gaming jokes and scripture on an hourly basis.
       There's not a lot else to see here.
  </body>
</html>
"""


class MainHandler(webapp2.RequestHandler):
    def get(self):
        self.response.write(MAIN_PAGE_HTML)

app = webapp2.WSGIApplication([
    ('/', MainHandler)
], debug=False)