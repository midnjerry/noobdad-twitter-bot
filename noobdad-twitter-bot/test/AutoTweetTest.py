import unittest
from google.appengine.ext import db
from google.appengine.ext import testbed
from google.appengine.datastore import datastore_stub_util
from autotweet import AutoTweet
from sets import Set

class TeamTestCase(unittest.TestCase):

  def setUp(self):
    # First, create an instance of the Testbed class.
    self.testbed = testbed.Testbed()
    # Then activate the testbed, which prepares the service stubs for use.
    self.testbed.activate()
    # Create a consistency policy that will simulate the High Replication consistency model.
    self.policy = datastore_stub_util.PseudoRandomHRConsistencyPolicy(probability=0)
    # Initialize the datastore stub with this policy.
    self.testbed.init_datastore_v3_stub(consistency_policy=self.policy)

  def tearDown(self):
    self.testbed.deactivate()

  
  def testAutoTweet_calculateCounter_returnsConsecutiveNumbersForAM_PM(self):
    year = 2017
    days = 36
    pictureSize = 425
    result1 = AutoTweet.calculateCounter(year, days, 11)
    result2 = AutoTweet.calculateCounter(year, days, 23)
    print ("%d | %d" % (result1 % pictureSize, result2 % pictureSize))
    # self.assertEqual(result1 + 1, result2)
  
  def testAutoTweet_TestCounter_AllNumbersReferenced(self):
    def calculateCountersForWholeYear(year, counterMap, pictureSize):
        for days in range(1, 366):
            for hours in range(1):
                counter = AutoTweet.calculateCounter(year, days, hours) % (pictureSize)
                if (counter == pictureSize):
                    print "Bad Counter %d" % (AutoTweet.calculateCounter(year, days, hours))
        
                if (counterMap.__contains__(counter)):
                    counterMap[counter] = counterMap.get(counter) + 1
                else:
                    counterMap[counter] = 1;
    
    year = 2017
    hours = 7
    pictureSize = 425
     
    counterMap = {}
    calculateCountersForWholeYear(2017, counterMap, pictureSize)
    calculateCountersForWholeYear(2018, counterMap, pictureSize)
    calculateCountersForWholeYear(2019, counterMap, pictureSize)
    calculateCountersForWholeYear(2020, counterMap, pictureSize)
    calculateCountersForWholeYear(2021, counterMap, pictureSize)
    calculateCountersForWholeYear(2022, counterMap, pictureSize)
    
               
    print(counterMap)
    
    max = 0
    min = 90000
    for k in counterMap.keys():
        if (counterMap.get(k) > max):
            max = counterMap.get(k)
        if (counterMap.get(k) < min):
            min = counterMap.get(k)    
    print("Max: %d  Min: %d" % (max,min))
    self.assertEqual(False, counterMap.__contains__(pictureSize))        
    
         
    
  def testEventuallyConsistentGlobalQueryResult(self):
    class TestModel(db.Model):
      pass

    user_key = db.Key.from_path('User', 'ryan')
    # Put two entities
    db.put([TestModel(parent=user_key), TestModel(parent=user_key)])

    # Global query doesn't see the data.
    self.assertEqual(0, TestModel.all().count(3))
    # Ancestor query does see the data.
    self.assertEqual(2, TestModel.all().ancestor(user_key).count(3))

if __name__ == '__main__':
    unittest.main()
