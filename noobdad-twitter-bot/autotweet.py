#!/usr/bin/env python

import fix_path
import os
#http://stackoverflow.com/questions/9604799/can-python-requests-library-be-used-on-google-app-engine?lq=1
from requests_toolbelt.adapters import appengine
appengine.monkeypatch()


import webapp2
import tweepy
import logging
import sys
import traceback
import random
import requests
from tweepy import *
from time import gmtime, strftime
from urllib2 import urlopen, URLError
from instapy_cli.cli import InstapyCli as client

def calculateCounter(year, days, hours):
    # creates a sequential counter based on time of call
    result =(int(year) * 365 * 2 + (int(days)-1) * 2 + (int(hours) / 12))
    # Multiplying by prime so pictures are not displayed consecutively - has the appearance of randomness
    result = result * 509
    return result

def calculateCounterFromTimestamp(num_seed):
    days = int(strftime("%j"))
    year = int(strftime("%Y"))
    hours = int(strftime("%H"))
    counter = calculateCounter(year, days, hours)
    counter += num_seed
    return counter

def calculateSeedFromName(user):
    num_seed = 0
    
    for c in user:
        num_seed += ord(c)-ord('a')
    
    return num_seed

class Follow(webapp2.RequestHandler):
    def get(self):
        try:
            user = self.request.get("user")
            with open(user + ".txt") as f:
                content = f.read().splitlines()
                # Consumer keys and access tokens, used for OAuth
                consumer_key = content[0]
                consumer_secret = content[1]
                access_token = content[2]
                access_token_secret = content[3]
            
            # OAuth process, using the keys and tokens
            self.response.write('Connecting to Twitter...<BR>')
            auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
            auth.set_access_token(access_token, access_token_secret)
             # Creation of the actual interface, using authentication
            api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True, compression=True)
         
            # Follow all users
            friends = tweepy.Cursor(api.friends_ids).items()
            followers = tweepy.Cursor(api.followers_ids).items()
            for id in followers:
                if id not in friends:
                    try:
                        api.create_friendship(id)
                        self.response.write("Following ID: " + str(id) + "<BR>")
                    except TweepError:
                        logging.info('TweepError: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
                        continue                
        
            self.response.write("Done.")
        except TweepError:
            logging.error('TweepError: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
        except URLError:
            logging.error('URLError: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
        except:
            logging.error('Unexpected error: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))

class CleanFollowers(webapp2.RequestHandler):
    def get(self):
        try:
            user = self.request.get("user")
            with open(user + ".txt") as f:
                content = f.read().splitlines()
                # Consumer keys and access tokens, used for OAuth
                consumer_key = content[0]
                consumer_secret = content[1]
                access_token = content[2]
                access_token_secret = content[3]
            
            # OAuth process, using the keys and tokens
            self.response.write('Connecting to Twitter...<BR>')
            auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
            auth.set_access_token(access_token, access_token_secret)
             # Creation of the actual interface, using authentication
            api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True, compression=True)
            
            followers = api.followers_ids(user)
            friends = api.friends_ids(user)
 
            for f in friends:
                if f not in followers:
                    api.destroy_friendship(f)
            
        except TweepError:
            logging.error('TweepError: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
        except URLError:
            logging.error('URLError: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
        except:
            logging.error('Unexpected error: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))

class MemeTweet(webapp2.RequestHandler):    
    def get(self):
        def generateCounter():
            try:
                counter = int(self.request.get("counter"))
            except ValueError:
                user = self.request.get("user")
                num_seed = calculateSeedFromName(user)
                self.response.write("Using seed " + str(num_seed)  +" for " + user + "<BR>")
                counter = calculateCounterFromTimestamp(num_seed)
            return counter
               
        counter = generateCounter()
        self.response.write("Counter:" + str(counter) + '<br>')
        
        try:
            user = self.request.get("user")
            with open(user + ".txt") as f:            
                content = f.read().splitlines()
                # Consumer keys and access tokens, used for OAuth
                consumer_key = content[0]
                consumer_secret = content[1]
                access_token = content[2]
                access_token_secret = content[3]
            
            # OAuth process, using the keys and tokens
            self.response.write('Connecting to Twitter...<BR>')
            auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
            auth.set_access_token(access_token, access_token_secret)
             # Creation of the actual interface, using authentication
            api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True, compression=True)
            
            def getFiles(path):
                directory = os.path.join(os.path.split(__file__)[0], path)
                listOfFiles = os.listdir(directory)
                return listOfFiles
            
            #Get jokes
            gameMemes = getFiles('images/games')
            gamesIndex = int(counter % len(gameMemes))
            self.response.write("gamesIndex: %d <BR>" % (gamesIndex))
            
            rawMessage = "- #GamerMeme courtesy of REVMC.NET! -\n"
            self.response.write("gamesIndex: %d <BR>" % (gamesIndex))
            rawMessage += "\n\n" + "#GAMING #NOOBS #NEWBS #OVER9000 #GAMERS #GamersRock #FUNNY #awesome #gamer #gaming #lol #nerd #gfuel #game #meme #dank"
                         
            
            #random.choice(['#OverMEME, #GAMING', '#NOOBS', '#NEWBS', '#OVER9000', '#GAMERS', '#GamersRock', '#FUNNY', '#awesome', '#gamer', '#gaming', '#lol', '#nerd', '#gfuel', '#game', '#gamingmeme', '#face', '#meme', '#memes', '#dank', '#dankmeme', '#GeeksUnite'])
            
            #Convert message to utf-8 so Tweepy doesn't strip newlines
            message = rawMessage.decode('utf-8')    
            self.response.write("Length of message " + str(len(message)))
                
            filename = os.path.join(os.path.split(__file__)[0], 'images/games', gameMemes[gamesIndex])
            self.response.write("Attempting tweet: " + filename + "<BR>")
            api.update_with_media(filename, message)
            self.response.write("Tweeted " + message + " Attachment: " + filename + "<BR>")
            
            logging.info(message)

        except TweepError:
            logging.error('TweepError: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
            logging.error(message)
        except URLError:
            logging.error('URLError: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
            logging.error(message)
        except:
            logging.error('Unexpected error: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))

class JokeTweet(webapp2.RequestHandler):    
    def get(self):
        def generateCounter():
            try:
                counter = int(self.request.get("counter"))
            except ValueError:
                user = self.request.get("user")
                num_seed = calculateSeedFromName(user)
                self.response.write("Using seed " + str(num_seed)  +" for " + user + "<BR>")
                counter = calculateCounterFromTimestamp(num_seed)
            return counter
               
        counter = generateCounter()
        self.response.write("Counter:" + str(counter) + '<br>')
        
        try:
            user = self.request.get("user")
            with open(user + ".txt") as f:            
                content = f.read().splitlines()
                # Consumer keys and access tokens, used for OAuth
                consumer_key = content[0]
                consumer_secret = content[1]
                access_token = content[2]
                access_token_secret = content[3]
            
            # OAuth process, using the keys and tokens
            self.response.write('Connecting to Twitter...<BR>')
            auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
            auth.set_access_token(access_token, access_token_secret)
             # Creation of the actual interface, using authentication
            api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True, compression=True)
            
            def getFiles(path):
                directory = os.path.join(os.path.split(__file__)[0], path)
                listOfFiles = os.listdir(directory)
                return listOfFiles
            
            with open("jokes.txt") as f:
            #with open ("images/files.txt") as f:
                content = f.read().splitlines()
                self.response.write("Opened file <BR>")
            
            jokesIndex = int(counter % len(content))
            self.response.write("JokesIndex: %d <BR>" % (jokesIndex))

            rawMessage = "- #GamerMeme courtesy of REVMC.NET! -\n"
            rawMessage += content[jokesIndex].replace("\\n", "\n") + " "
            rawMessage += "\n\n" + "#GAMING #NOOBS #NEWBS #OVER9000 #GAMERS #GamersRock #FUNNY #awesome #gamer #gaming #lol #nerd #gfuel #game #meme #dank"
            #Convert message to utf-8 so Tweepy doesn't strip newlines
            message = rawMessage.decode('utf-8')    
            self.response.write("Length of message " + str(len(message)))
            self.response.write("Attempting tweet: <BR>")
            api.update_status(message)
            self.response.write("Tweeted " + message + "<BR>")
            logging.info(message)
        except TweepError:
            logging.error('TweepError: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
            logging.error(message)
        except URLError:
            logging.error('URLError: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
            logging.error(message)
        except:
            logging.error('Unexpected error: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
            
class JesusTweet(webapp2.RequestHandler):    
    def get(self):
        def generateCounter():
            try:
                counter = int(self.request.get("counter"))
            except ValueError:
                user = self.request.get("user")
                num_seed = calculateSeedFromName(user)
                self.response.write("Using seed " + str(num_seed)  +" for " + user + "<BR>")
                counter = calculateCounterFromTimestamp(num_seed)
            return counter
               
        counter = generateCounter()
        self.response.write("Counter:" + str(counter) + '<br>')
        
        try:
            user = self.request.get("user")
            with open(user + ".txt") as f:            
                content = f.read().splitlines()
                # Consumer keys and access tokens, used for OAuth
                consumer_key = content[0]
                consumer_secret = content[1]
                access_token = content[2]
                access_token_secret = content[3]
            
            # OAuth process, using the keys and tokens
            self.response.write('Connecting to Twitter...<BR>')
            auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
            auth.set_access_token(access_token, access_token_secret)
             # Creation of the actual interface, using authentication
            api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True, compression=True)
            
            def getFiles(path):
                directory = os.path.join(os.path.split(__file__)[0], path)
                listOfFiles = os.listdir(directory)
                return listOfFiles
            
            bibleImages = getFiles('images/bible')
            bibleIndex = int(counter % len(bibleImages))
            self.response.write("bibleIndex: %d <BR>" % (bibleIndex))
            rawMessage = "- Have a #BLESSED DAY courtesy of REVMC.NET -\n\n\n"
            rawMessage += "#GodRules #GodIsLove #GodIsAwesome #Awesome #Love #LoveTrumpsHate #Beautiful #Salvation #JesusChrist #Jesus #KingOfKings #LordOfLords #JesusIsKing #Fuel #ChristHasRisen"
            
            #rawMessage += random.choice(['#GodRules, #GodIsLove', '#GodIsAwesome', '#Awesome', '#Love', '#LoveTrumpsHate', '#Beautiful', '#Salvation', '#JesusChrist', '#Jesus', '#KingOfKings', '#LordOfLords', '#JesusIsKing', '#Fuel', '#ChristHasRisen'])            
            
            #Convert message to utf-8 so Tweepy doesn't strip newlines
            message = rawMessage.decode('utf-8')
            filename = os.path.join(os.path.split(__file__)[0], 'images/bible', bibleImages[bibleIndex])
            self.response.write("Attempting tweet: " + filename + "<BR>")
            api.update_with_media(filename, message)
            self.response.write("Tweeted " + message + " Attachment: " + filename + "<BR>")
            logging.info(message)

        except TweepError:
            logging.error('TweepError: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
            logging.error(message)
        except URLError:
            logging.error('URLError: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
            logging.error(message)
        except:
            logging.error('Unexpected error: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
            
class AnimeTweet(webapp2.RequestHandler):    
    def get(self):
        def generateCounter():
            try:
                counter = int(self.request.get("counter"))
            except ValueError:
                user = self.request.get("user")
                num_seed = calculateSeedFromName(user)
                self.response.write("Using seed " + str(num_seed)  +" for " + user + "<BR>")
                counter = calculateCounterFromTimestamp(num_seed)
            return counter
               
        counter = generateCounter()
        self.response.write("Counter:" + str(counter) + '<br>')
        
        try:
            user = self.request.get("user")
            with open(user + ".txt") as f:            
                content = f.read().splitlines()
                # Consumer keys and access tokens, used for OAuth
                consumer_key = content[0]
                consumer_secret = content[1]
                access_token = content[2]
                access_token_secret = content[3]
            
            # OAuth process, using the keys and tokens
            self.response.write('Connecting to Twitter...<BR>')
            auth = tweepy.OAuthHandler(consumer_key, consumer_secret)
            auth.set_access_token(access_token, access_token_secret)
             # Creation of the actual interface, using authentication
            api = tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True, compression=True)
            
            def getFiles(path):
                directory = os.path.join(os.path.split(__file__)[0], path)
                listOfFiles = os.listdir(directory)
                return listOfFiles
            
            #Get jokes
            gameMemes = getFiles('images/anime')
            gamesIndex = int(counter % len(gameMemes))
            self.response.write("animeIndex: %d <BR>" % (gamesIndex))
            
            rawMessage = "- #AnimeMEME courtesy of REVMC.NET! -\n"
            self.response.write("gamesIndex: %d <BR>" % (gamesIndex))
            rawMessage += "\n\n" + "#GAMING #NOOBS #NEWBS #OVER9000 #GAMERS #ANIME #FUNNY #awesome #gamer #gaming #lol #nerd #gfuel #game #meme #dank #WEEB"
                         
            
            #random.choice(['#OverMEME, #GAMING', '#NOOBS', '#NEWBS', '#OVER9000', '#GAMERS', '#GamersRock', '#FUNNY', '#awesome', '#gamer', '#gaming', '#lol', '#nerd', '#gfuel', '#game', '#gamingmeme', '#face', '#meme', '#memes', '#dank', '#dankmeme', '#GeeksUnite'])
            
            #Convert message to utf-8 so Tweepy doesn't strip newlines
            message = rawMessage.decode('utf-8')    
            self.response.write("Length of message " + str(len(message)))
                
            filename = os.path.join(os.path.split(__file__)[0], 'images/anime', gameMemes[gamesIndex])
            self.response.write("Attempting tweet: " + filename + "<BR>")
            api.update_with_media(filename, message)
            self.response.write("Tweeted " + message + " Attachment: " + filename + "<BR>")
            
            logging.info(message)

        except TweepError:
            logging.error('TweepError: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
            logging.error(message)
        except URLError:
            logging.error('URLError: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))
            logging.error(message)
        except:
            logging.error('Unexpected error: ' + str(sys.exc_info()[0]) + str(sys.exc_info()[1]))                        
            
app = webapp2.WSGIApplication([ 
    ('/auto_tweet', MemeTweet),
    ('/auto_joke', JokeTweet),
    ('/auto_jesus', JesusTweet),
    ('/auto_anime', AnimeTweet),                        
    ('/clean', CleanFollowers),
    ('/follow', Follow)           
], debug=False)

